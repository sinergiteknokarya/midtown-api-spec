# OPEN API SPEC MIDTOWN MOBILE

Dalam dokumen ini menggunkan format yaml, untuk midtown indonesia berikut yang bisa dilakukan untuk dapat membaca atau menjalankan dokumentasi tersebut.

## menggunkan library Redocly untuk generate dokumen

Jalankan perintah ini di terminal. dengan catatan software pendukung sudah terpasang
   

**install redocly**
```console
npm install @redocly/cli -g
   ```
**untuk melakukan validasi struktur dokument**
```console
redocly lint main.yaml --extends minimal --format json
```
**untuk generate dokument file html**
```console
redocly build-docs main.yaml
```
**untuk membuat satu file utuh dengan format json**
```console
redocly bundle main.yaml --output openapi.json
```	
**untuk membuat satu file utuh dengan format yaml**
```console
redocly bundle main.yaml --output openapi.yaml
```	

**untuk menjalankan file main.yaml dan di akses di lokal browser**
```console
redocly preview-docs main.yaml
```

**create mock server**
```console
npm install -g @stoplight/prism-cli
```

**runing mock server**
```console
prism mock openapi.yaml
prism mock openapi.yaml& //run on background
```